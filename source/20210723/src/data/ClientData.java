package data;

import control.ClientCommanding;
import io.Input;
import io.Output;
import java.net.Socket;

public class ClientData {
    public Input in;
    public Output out;
    public ClientCommanding cmd;
    public String address;
    public int prt;
    public String registerName;
    public Socket client;
    public boolean muted;
}
