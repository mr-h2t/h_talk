package data;

import io.Input;
import io.Output;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.List;
import java.util.Map;


public class ServerData{
    public Output out;
    public Input in;
    public ServerSocket serverSocket;
    public Map<String, Socket> clientList;
    public Map<String,Boolean> banList;
    public Map<String,ClientInfo> infoList;
    public Map<String,Boolean> ipBanList;
    public Map<String,Boolean> muteList;
    public List<Socket> preparingSocket;
    public boolean serverStop;
}
