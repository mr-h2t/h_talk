package data;

public class ClientInfo{
    public String remoteIP;
    public int localPort;
    public ClientInfo(String rIP, int lP){
        remoteIP=rIP;
        localPort=lP;
    }
}