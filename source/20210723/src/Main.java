import gui.ClientFrame;
import gui.RegisterFrame;
import gui.ServerFrame;

import java.io.IOException;

public class Main {
    final int port=1235;
    public static void main(String[] args)throws IOException {
        Main m=new Main();
//        char c= (char) System.in.read();
        if(args.length==0){
            m.goServer();
        }
        if(args.length!=0){

            String c=new String();
            c=args[0];
            if(c.equals("1"))m.goClient();
            if(c.equals("2"))m.goServer();
        }
    }
    public void goClient() {
        //需要服务器的正确的IP地址和端口号
        new RegisterFrame();
    }
    public void goServer() throws IOException {
        new ServerFrame(port);
    }
}
