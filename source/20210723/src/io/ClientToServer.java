package io;

import data.ClientData;
import data.CurDate;
import gui.DialogFrame;

import java.io.IOException;
import java.io.PrintStream;

public class ClientToServer {
    ClientData data;
    PrintStream ps;
    public ClientToServer(ClientData data) {
        super();
        this.data=data;
    }
    public void registerUser(String registerName){
        ps.println("$REGISTER$"+registerName);
    }
    public void initPrintStream(){
        try {
            ps = new PrintStream(data.client.getOutputStream(), true);
        }catch(Exception e){}
    }
    public void logoff(){
        try {
            data.out.println("客户端退出");
            ps.println("$LOGOFF$");
            Thread.sleep(1000);
            data.client.close();
            ps.close();
            System.exit(0);
        }catch(Exception e){}
    }
    public void send(String string) {
        if(string.equals(""))return;
        if(data.muted){
            DialogFrame.showErrorDialog("您已被服务器禁言");
            return;
        }
        this.data.out.println(CurDate.getDate()+"\n我:"+string);
        ps.println("$MESSAGE$"+string);
    }
}
