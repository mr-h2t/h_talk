package io;

abstract public class Output {
    public void print(String msg){}
    public void println(String msg){print(msg+'\n');}
}
