package gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class RegisterFrame extends JFrame {
    JTextField inputBox=new JTextField();
    JTextField usernameBox=new JTextField();
    JLabel text1=new JLabel("IP+端口(冒号分隔):"),text2=new JLabel("注册名:");
    private void register(){
        String rmIP=inputBox.getText();
        inputBox.setText("");
        if(!rmIP.matches("\\d+\\.\\d+\\.\\d+\\.\\d+\\:\\d+")){
            inputBox.setText("非法格式");
            return;
        }
        if(usernameBox.getText().equals("")){
            inputBox.setText("未输入用户名");
            return;
        }
        this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        this.setVisible(false);
        this.dispose();
        String prt=rmIP.split("\\:")[1];
        int port=0;
        for(int i=0;i<prt.length();i++)port=port*10+(int)prt.charAt(i)-'0';

        new ClientFrame(rmIP.split("\\:")[0],port,usernameBox.getText());
    }
    public RegisterFrame(){
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setResizable(false);
        this.setLayout(null);
        inputBox.addKeyListener(new KeyListener() {
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    register();
                }
            }
            public void keyReleased(KeyEvent e) {
            }
            public void keyTyped(KeyEvent e) {
            }
        });
        text1.setBounds(10,50,120,30);
        inputBox.setBounds(120,50,180,30);
        text2.setBounds(10,10,100,30);
        usernameBox.setBounds(120,10,180,30);
        Container con=this.getContentPane();
        con.add(text1);con.add(inputBox);con.add(text2);con.add(usernameBox);
        this.setBounds(10,10,320,120);
        this.setTitle("H_Talk 注册界面");
        this.setVisible(true);
    }
}
