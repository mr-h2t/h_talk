package gui;

import control.ClientCommanding;
import data.ClientData;
import io.ClientToServer;
import io.Output;
import soc.Client;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import static java.awt.GridBagConstraints.BOTH;
import static java.awt.GridBagConstraints.HORIZONTAL;


public class ClientFrame extends JFrame {
    ClientToServer cts;
    Client client;
    JTextField inputBox=new JTextField();
    JTextArea consoleBox=new JTextArea();
    ClientData data=new ClientData();
    class ClientOutput extends Output {
        @Override
        public void print(String msg){
            consoleBox.setText(consoleBox.getText()+msg);
            consoleBox.setCaretPosition(consoleBox.getText().length());
        }
    }
    public ClientFrame(String ip, int prt,String registerName)
    {

        dataInit(ip,prt,registerName);
        this.cts=new ClientToServer(this.data);
        this.data.cmd=new ClientCommanding(this.data,this.cts);
        client=new Client(data, cts);

        this.setBounds(10,10,500,600);

        this.setLayout(new GridBagLayout());

        this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                cts.logoff();
                super.windowClosing(e);
            }
        });


        Container con=this.getContentPane();
        consoleBox.setEditable(false);
        GridBagConstraints consoleBoxGrid=new GridBagConstraints();
        consoleBoxGrid.gridx=0;
        consoleBoxGrid.gridy=0;
        consoleBoxGrid.gridwidth=3;
        consoleBoxGrid.gridheight=2;
        consoleBoxGrid.fill=BOTH;
        consoleBoxGrid.weightx=100;
        consoleBoxGrid.weighty=100;
        consoleBoxGrid.insets=new Insets(10,10,10,10);

        JScrollPane consoleScrollPane=new JScrollPane(consoleBox);
        con.add(consoleScrollPane,consoleBoxGrid);

        GridBagConstraints inputBoxGrid=new GridBagConstraints();
        inputBoxGrid.gridx=0;
        inputBoxGrid.gridy=2;
        inputBoxGrid.gridwidth=3;
        inputBoxGrid.gridheight=1;
        inputBoxGrid.fill=HORIZONTAL;
        inputBoxGrid.weightx=100;
        inputBoxGrid.weighty=0;
        inputBoxGrid.insets=new Insets(10,10,10,10);
        con.add(inputBox,inputBoxGrid);

        inputBox.addKeyListener(new KeyListener() {
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    String v=inputBox.getText();
                    inputBox.setText("");
                    if(v.startsWith("-")){
                        data.cmd.compileCommand(v);
                    }
                    else cts.send(v);
                }
            }
            public void keyReleased(KeyEvent e) {
            }
            public void keyTyped(KeyEvent e) {
            }
        });

        this.setTitle("H_Talk 客户端");
        this.setVisible(true);
    }
    private void dataInit(String ip, int prt,String registerName){
        this.data.out=new ClientOutput();
        this.data.address=ip;
        this.data.prt=prt;
        this.data.registerName=registerName;
    }
}
