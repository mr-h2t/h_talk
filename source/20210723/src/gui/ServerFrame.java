package gui;

import control.ServerCommanding;
import data.ServerData;
import soc.Server;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.HashMap;

import static java.awt.GridBagConstraints.BOTH;
import static java.awt.GridBagConstraints.HORIZONTAL;

public class ServerFrame extends JFrame {
    Server server;

    JTextField inputBox=new JTextField();
    JTextArea consoleBox=new JTextArea();
    class serverInput extends io.Input{
        @Override
        public String read(){

            return null;
        }
    }
    class serverOutput extends io.Output {
        @Override
        public void print(String msg){
            consoleBox.setText(consoleBox.getText()+msg);
            consoleBox.setCaretPosition(consoleBox.getText().length());
        }
    }
    public ServerFrame(int prt) throws IOException {

        this.setLayout(new GridBagLayout());
        this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                cmd.compileCommand("-shutdown");
                super.windowClosing(e);

            }
        });
        Container con=this.getContentPane();

        GridBagConstraints consoleBoxGrid=new GridBagConstraints();
        consoleBoxGrid.gridx=0;
        consoleBoxGrid.gridy=0;
        consoleBoxGrid.gridwidth=3;
        consoleBoxGrid.gridheight=2;
        consoleBoxGrid.fill=BOTH;
        consoleBoxGrid.weightx=100;
        consoleBoxGrid.weighty=100;
        consoleBoxGrid.insets=new Insets(10,10,10,10);
        consoleBox.setEditable(false);

        JScrollPane consoleScrollPane=new JScrollPane(consoleBox);
        con.add(consoleScrollPane,consoleBoxGrid);

        GridBagConstraints inputBoxGrid=new GridBagConstraints();
        inputBoxGrid.gridx=0;
        inputBoxGrid.gridy=2;
        inputBoxGrid.gridwidth=3;
        inputBoxGrid.gridheight=1;
        inputBoxGrid.fill=HORIZONTAL;
        inputBoxGrid.weightx=100;
        inputBoxGrid.weighty=0;
        inputBoxGrid.insets=new Insets(10,10,10,10);
        con.add(inputBox,inputBoxGrid);

        inputBox.addKeyListener(new KeyListener() {
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    String v=inputBox.getText();
                    if(v.equals("")||!v.startsWith("-"))return;//无 - 同等与无效
                    inputBox.setText("");
                    cmd.compileCommand(v);
                }
            }
            public void keyReleased(KeyEvent e) {
            }
            public void keyTyped(KeyEvent e) {
            }
        });
        this.setBounds(10,10,500,600);
        this.setTitle("H_Talk 服务器端");

        serverInit(prt);
        this.setVisible(true);
    }
    private ServerCommanding cmd;
    private ServerData data=new ServerData();
    void serverInit(int prt) throws IOException {
        data.serverSocket=new ServerSocket(prt);
        data.clientList =new HashMap<>();
        data.banList=new HashMap<>();
        data.infoList=new HashMap<>();
        data.ipBanList=new HashMap<>();
        data.muteList=new HashMap<>();
        data.preparingSocket=new ArrayList<>();
        data.in=new serverInput();
        data.out=new serverOutput();
        data.serverStop=false;

        server=new Server(data);
        cmd=new ServerCommanding(data);
    }
}
