package control.serverCommands;

import data.ServerData;

import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Map;

public class CommandStart extends Command{
    public CommandStart(ServerData data){
        super(data);
        this.commandName="start";
        this.commandInfo="重新对外开放服务器 -start";
    }
    void sendToAll(String msg) {
        for (Map.Entry<String, Socket> stringSocketEntry : data.clientList.entrySet()) {
            try {
                Socket socket = stringSocketEntry.getValue();
                PrintStream printStream = new PrintStream(socket.getOutputStream(), true);
                printStream.println(msg);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        for (Socket s : data.preparingSocket) {
            try {
                PrintStream printStream = new PrintStream(s.getOutputStream(), true);
                printStream.println(msg);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    @Override
    public void commandRun(){
        this.data.serverStop=false;
        data.out.println("服务器重新对外开放");
        sendToAll("[服务器重新对外开放]");
    }
}
