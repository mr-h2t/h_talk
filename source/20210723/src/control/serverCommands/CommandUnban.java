package control.serverCommands;

import data.ServerData;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommandUnban extends Command{
    public CommandUnban(ServerData data){
        super(data);
        this.commandName="unban";
        this.commandInfo="解除对用户的封禁 -unban @Username";
    }
    @Override
    public void commandRun(String command){
        Pattern p = Pattern.compile("unban ");
        Matcher m = p.matcher(command);
        String username = m.replaceAll("");
        data.banList.remove(username);
        String targetIP=data.infoList.get(username).remoteIP;
        if(data.ipBanList.get(targetIP)!=null)data.ipBanList.remove(targetIP);
        data.out.println("已解封用户" + username);
    }
}
