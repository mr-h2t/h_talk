package control.serverCommands;

import data.ServerData;

import java.util.Map;

public class CommandBannedIP extends Command{
    public CommandBannedIP(ServerData data){
        super(data);
        this.commandName="bannedip";
        this.commandInfo="检查被封禁IP -bannedip";
    }
    void checkIPBannedList() {
        data.out.println("有" + data.ipBanList.size() + "个IP被封禁");
        for (Map.Entry<String, Boolean> stringSocketEntry : data.ipBanList.entrySet()) {
            data.out.println(stringSocketEntry.getKey());
        }
    }
    @Override
    public void commandRun(){
        checkIPBannedList();
    }

}
