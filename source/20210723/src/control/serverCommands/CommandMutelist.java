package control.serverCommands;

import data.ServerData;

import java.net.Socket;
import java.util.Map;

public class CommandMutelist extends Command{
    public CommandMutelist(ServerData data){
        super(data);
        this.commandName="mutelist";
        this.commandInfo="检查被禁言用户 -mutelist";
    }
    void checkMuteList(){
        data.out.println("当前被禁言人数:" + data.muteList.size());
        for (Map.Entry<String, Boolean> stringSocketEntry : data.muteList.entrySet()) {
            data.out.println(stringSocketEntry.getKey());
        }
    }
    @Override
    public void commandRun(){
        checkMuteList();
    }
}
