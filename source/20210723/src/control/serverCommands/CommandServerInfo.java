package control.serverCommands;

import data.ServerData;

public class CommandServerInfo extends Command{
    public CommandServerInfo(ServerData data){
        super(data);
        this.commandName="serverinfo";
        this.commandInfo="查看服务器信息 -serverinfo";
    }
    void checkServerInfo(){
        data.out.println("服务器IP:"+data.serverSocket.getInetAddress());
        data.out.println("服务器端口:"+data.serverSocket.getLocalPort());
    }
    @Override
    public void commandRun(){
        checkServerInfo();
    }
}
