package control.serverCommands;

import data.ServerData;

import java.io.IOException;

public class Command{
    ServerData data;
    public Command(ServerData data){
        this.data=data;
    }
    String commandName;
    String commandInfo;
    public String getCommandName(){return commandName;}
    public String getCommandInfo(){return commandInfo;}
    public void commandRun(){

    }
    public void commandRun(String command){}
}

