package control.serverCommands;

import data.ServerData;

import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommandNotice extends Command{
    public CommandNotice(ServerData data){
        super(data);
        this.commandName="notice";
        this.commandInfo="发布公告 -notice @Message";
    }
    void sendToAll(String msg) {
        for (Map.Entry<String, Socket> stringSocketEntry : data.clientList.entrySet()) {
            try {
                Socket socket = stringSocketEntry.getValue();
                PrintStream printStream = new PrintStream(socket.getOutputStream(), true);
                printStream.println(msg);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        for (Socket s : data.preparingSocket) {
            try {
                PrintStream printStream = new PrintStream(s.getOutputStream(), true);
                printStream.println(msg);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    @Override
    public void commandRun(String command){
        Pattern p = Pattern.compile("notice ");
        Matcher m = p.matcher(command);
        String noticeMsg = m.replaceAll("");
        data.out.println("发布公告:"+noticeMsg);
        sendToAll("[服务器公告]:" + noticeMsg);
    }
}
