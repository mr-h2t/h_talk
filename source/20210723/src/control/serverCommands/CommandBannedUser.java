package control.serverCommands;

import data.ServerData;

import java.util.Map;

public class CommandBannedUser extends Command{
    public CommandBannedUser(ServerData data){
        super(data);
        this.commandName = "banneduser";
        this.commandInfo="检查被封禁用户 -banneduser";

    }
    void checkBannedList() {
        data.out.println("有" + data.banList.size() + "名用户被封禁");
        for (Map.Entry<String, Boolean> stringSocketEntry : data.banList.entrySet()) {
            data.out.println(stringSocketEntry.getKey());
        }
    }
    @Override
    public void commandRun(){
        checkBannedList();
    }
}
