package control.serverCommands;

import data.ServerData;

import java.io.PrintStream;
import java.net.Socket;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommandUnmute extends Command{
    public CommandUnmute(ServerData data){
        super(data);
        this.commandName="unmute";
        this.commandInfo="解除用户禁言 -unmute @Username";
    }
    @Override
    public void commandRun(String command){
        Pattern p = Pattern.compile("unmute ");
        Matcher m = p.matcher(command);
        String username = m.replaceAll("");
        data.muteList.remove(username);
        data.out.println("解除对用户" + username+"的禁言");
        try{
            Socket target=data.clientList.get(username);
            PrintStream printStream = new PrintStream(target.getOutputStream(), true);
            printStream.println("$UNMUTED$");
        }catch(Exception e){}
    }
}
