package control.serverCommands;

import data.ServerData;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommandUnbanIP extends Command{
    public CommandUnbanIP(ServerData data){
        super(data);
        this.commandName="unbanip";
        this.commandInfo="解除对IP地址的封禁 -unbanip @IPAddress";
    }
    @Override
    public void commandRun(String command){
        Pattern p = Pattern.compile("unbanip ");
        Matcher m = p.matcher(command);
        String targetIP = m.replaceAll("");
        data.ipBanList.remove("/" + targetIP);
        data.out.println("IP " + targetIP + "已解封");
    }
}
