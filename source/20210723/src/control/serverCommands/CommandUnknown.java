package control.serverCommands;

import data.ServerData;

public class CommandUnknown extends Command{
    public CommandUnknown(ServerData data){
        super(data);
        this.commandName="unknown";
        this.commandInfo="未知命令或错误格式";
    }
    @Override
    public void commandRun(String command){
        data.out.println("未知指令或错误格式:"+command);
    }
}
