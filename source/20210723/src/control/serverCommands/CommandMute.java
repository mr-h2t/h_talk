package control.serverCommands;

import data.ServerData;

import java.io.PrintStream;
import java.net.Socket;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommandMute extends Command{
    public CommandMute(ServerData data){
        super(data);
        this.commandName="mute";
        this.commandInfo="禁言某一用户 -mute @Username";
    }
    @Override
    public void commandRun(String command){
        Pattern p = Pattern.compile("mute ");
        Matcher m = p.matcher(command);
        String username = m.replaceAll("");
        data.muteList.put(username,true);
        data.out.println("已禁言用户"+username);
        try{
            Socket target=data.clientList.get(username);
            PrintStream printStream = new PrintStream(target.getOutputStream(), true);
            printStream.println("$MUTED$");
        }catch(Exception e){}
    }
}
