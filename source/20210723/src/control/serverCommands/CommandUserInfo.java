package control.serverCommands;

import data.ServerData;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommandUserInfo extends Command{
    public CommandUserInfo(ServerData data){
        super(data);
        this.commandName="userinfo";
        this.commandInfo="检查成员信息 -userinfo @Username";
    }
    void checkUser(String username) {
        data.out.println("用户" + username + ":");
        if (data.infoList.get(username) == null) {
            data.out.println("未登录过");
            data.out.print("用户状态:");
            if (data.banList.get(username) == null) {
                data.out.println("正常");
            } else data.out.println("已封禁");
            return;
        }
        data.out.println("用户IP(最近登录):" + data.infoList.get(username).remoteIP);
        data.out.println("用户端口(最近登录):" + data.infoList.get(username).localPort);
        data.out.print("用户状态:");
        if (data.banList.get(username) == null) {
            data.out.println("正常");
        } else data.out.println("已封禁");
    }
    @Override
    public void commandRun(String command){
        Pattern p = Pattern.compile("userinfo ");
        Matcher m = p.matcher(command);
        String username = m.replaceAll("");
        checkUser(username);
    }
}
