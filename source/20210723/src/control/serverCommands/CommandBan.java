package control.serverCommands;

import data.ServerData;

import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommandBan extends Command{
    public CommandBan(ServerData data){
        super(data);
        this.commandName="ban";
        this.commandInfo="封禁用户（名） -ban @Username";
    }
    void sendToAll(String msg) {
        for (Map.Entry<String, Socket> stringSocketEntry : data.clientList.entrySet()) {
            try {
                Socket socket = stringSocketEntry.getValue();
                PrintStream printStream = new PrintStream(socket.getOutputStream(), true);
                printStream.println(msg);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        for (Socket s : data.preparingSocket) {
            try {
                PrintStream printStream = new PrintStream(s.getOutputStream(), true);
                printStream.println(msg);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    boolean kick(String userName) throws IOException {
        Socket target = data.clientList.get(userName);
        if (target == null) return false;
        PrintStream printStream = new PrintStream(target.getOutputStream(), true);
        printStream.println("$KICKED$");
        target.close();
        data.clientList.remove(userName);
        return true;
    }
    @Override
    public void commandRun(String command){
        Pattern p = Pattern.compile("ban ");
        Matcher m = p.matcher(command);
        String username = m.replaceAll("");

        data.banList.put(username, true);
        try {
            kick(username);
            data.out.println("已封禁用户" + username);
            sendToAll("[用户"+username+"已被封禁]");
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(data.infoList.get(username)!=null){
            String targetIP=data.infoList.get(username).remoteIP;
            data.ipBanList.put(targetIP,true);
        }
    }
}
