package control.serverCommands;

import data.ServerData;

import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommandBanIP extends Command{
    public CommandBanIP(ServerData data){
        super(data);
        this.commandName="banip";
        this.commandInfo="封禁IP地址 -banip @IPAddress";
    }
    void sendToAll(String msg) {
        for (Map.Entry<String, Socket> stringSocketEntry : data.clientList.entrySet()) {
            try {
                Socket socket = stringSocketEntry.getValue();
                PrintStream printStream = new PrintStream(socket.getOutputStream(), true);
                printStream.println(msg);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        for (Socket s : data.preparingSocket) {
            try {
                PrintStream printStream = new PrintStream(s.getOutputStream(), true);
                printStream.println(msg);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    @Override
    public void commandRun(String command){
        Pattern p = Pattern.compile("banip ");
        Matcher m = p.matcher(command);
        String targetIP = m.replaceAll("");
        data.ipBanList.put("/" + targetIP, true);
        for (Map.Entry<String, Socket> stringSocketEntry : data.clientList.entrySet()) {
            Socket v = stringSocketEntry.getValue();
            if (v.getInetAddress().toString().equals("/" + targetIP)) {
                try {
                    PrintStream printStream = new PrintStream(v.getOutputStream(), true);
                    printStream.println("$KICKED$");
                    v.close();
                    data.clientList.remove(stringSocketEntry.getKey());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        List<Socket> tmp = new ArrayList<>();
        for (Socket s : data.preparingSocket) {
            if (s.getInetAddress().toString().equals("/" + targetIP)) {
                try {
                    PrintStream printStream = new PrintStream(s.getOutputStream(), true);
                    printStream.println("$KICKED$");
                    s.close();
                    tmp.add(s);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        for (Socket s : tmp) data.preparingSocket.remove(s);

        data.out.println("IP " + targetIP + "已封禁");
        sendToAll("[IP "+targetIP+"已被封禁]");
    }
}
