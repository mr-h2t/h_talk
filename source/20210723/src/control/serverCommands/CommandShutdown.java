package control.serverCommands;

import data.ServerData;

import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Map;

public class CommandShutdown extends Command{
    public CommandShutdown(ServerData data){
        super(data);
        this.commandName="shutdown";
        this.commandInfo="彻底关闭服务器 -shutdown";
    }
    void sendToAll(String msg) {
        for (Map.Entry<String, Socket> stringSocketEntry : data.clientList.entrySet()) {
            try {
                Socket socket = stringSocketEntry.getValue();
                PrintStream printStream = new PrintStream(socket.getOutputStream(), true);
                printStream.println(msg);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        for (Socket s : data.preparingSocket) {
            try {
                PrintStream printStream = new PrintStream(s.getOutputStream(), true);
                printStream.println(msg);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    void sendShutdownMessage() {
        data.out.println("服务器已关闭!!!");
        sendToAll("$SHUTDOWN$");
    }
    @Override
    public void commandRun(){
        try {
            sendShutdownMessage();
            data.serverSocket.close();
            System.exit(0);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
