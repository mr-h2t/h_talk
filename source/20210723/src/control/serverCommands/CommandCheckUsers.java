package control.serverCommands;

import data.ServerData;

import java.net.Socket;
import java.util.Map;

public class CommandCheckUsers extends Command{
    public CommandCheckUsers(ServerData data){
        super(data);
        this.commandName="checkusers";
        this.commandInfo="检查当前在线成员 -checkusers";
    }
    void checkUsers() {
        data.out.println("当前在线人数:" + data.clientList.size());
        for (Map.Entry<String, Socket> stringSocketEntry : data.clientList.entrySet()) {
            data.out.println(stringSocketEntry.getKey());
        }
    }
    @Override
    public void commandRun(){
        checkUsers();
    }
}
