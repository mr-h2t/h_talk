package control;

import control.serverCommands.*;
import data.CurDate;
import data.ServerData;

import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class ServerCommanding {
    ServerData data;

    ArrayList<Command> registeredCommands;
    Command unknownCommand;
    public ServerCommanding(ServerData dat) {
        data = dat;
        registerCommands();
    }

    Command getCommand(String commandName) {
        for (Command i : registeredCommands) {
            if (i.getCommandName().equals(commandName)) return i;
        }
        return unknownCommand;
    }
    void registerCommands(){
        this.registeredCommands=new ArrayList<>();
        this.registeredCommands.add(new CommandBan(data));
        this.registeredCommands.add(new CommandBanIP(data));
        this.registeredCommands.add(new CommandBannedIP(data));
        this.registeredCommands.add((new CommandBannedUser(data)));
        this.registeredCommands.add(new CommandCheckUsers(data));
        this.registeredCommands.add(new CommandKick(data));
        this.registeredCommands.add(new CommandMute(data));
        this.registeredCommands.add(new CommandMutelist(data));
        this.registeredCommands.add(new CommandNotice(data));
        this.registeredCommands.add(new CommandServerInfo(data));
        this.registeredCommands.add(new CommandShutdown(data));
        this.registeredCommands.add(new CommandStart(data));
        this.registeredCommands.add(new CommandStop(data));
        this.registeredCommands.add(new CommandUnban(data));
        this.registeredCommands.add(new CommandUnbanIP(data));
        this.registeredCommands.add(new CommandUnmute(data));
        this.registeredCommands.add(new CommandUserInfo(data));


        this.unknownCommand=new CommandUnknown(data);
    }
    void showHelpList(){
        data.out.println("本服务器可用命令如下:");
        for(Command i:registeredCommands){
            data.out.println("-"+i.getCommandName());
        }
    }
    void showHelp(String cmd){
        data.out.println("关于命令"+cmd+":");
        data.out.println(getCommand(cmd).getCommandInfo());
    }
    public void compileCommand(String command) {
        data.out.println(CurDate.getDate());
        command=command.split("\\-",2)[1];
        if (command.startsWith("shutdown")) {
            getCommand("shutdown").commandRun();
        } else if (command.startsWith("kick ")) {
            getCommand("kick").commandRun(command);
        } else if (command.startsWith("checkusers")) {
            getCommand("checkusers").commandRun();
        } else if (command.startsWith("userinfo ")) {
            getCommand("userinfo").commandRun(command);
        } else if (command.startsWith("notice ")) {
            getCommand("notice").commandRun(command);
        } else if (command.startsWith("ban ")) {
            getCommand("ban").commandRun(command);
        } else if (command.startsWith("unban ")) {
            getCommand("unban").commandRun(command);
        } else if (command.startsWith("banip ")) {
            getCommand("banip").commandRun(command);
        } else if (command.startsWith("unbanip ")) {
            getCommand("unbanip").commandRun(command);
        } else if (command.startsWith("banneduser")) {
            getCommand("banneduser").commandRun();
        } else if (command.startsWith("bannedip")) {
            getCommand("bannedip").commandRun();
        }else if(command.startsWith("serverinfo")){
            getCommand("serverinfo").commandRun();
        }else if(command.startsWith("stop")){
            getCommand("stop").commandRun();
        }else if(command.startsWith("start")){
            getCommand("start").commandRun();
        }else if(command.startsWith("mute ")){
            getCommand("mute").commandRun(command);
        }else if(command.startsWith("unmute ")){
            getCommand("unmute").commandRun(command);
        }else if(command.startsWith("mutelist ")){
            getCommand("mutelist").commandRun();
        }
        else if(command.startsWith("help")){
            Pattern p=Pattern.compile(" ");
            Matcher m= p.matcher(command);
            String simplified=m.replaceAll("");
            if(simplified.equals("help"))showHelpList();
            else {
                p=Pattern.compile("help");
                m=p.matcher(simplified);
                showHelp(m.replaceAll(""));
            }
        }
        else getCommand("unknown").commandRun();
    }
}