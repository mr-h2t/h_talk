package soc;

import data.ClientInfo;
import data.CurDate;
import data.ServerData;
import java.io.IOException;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
class ExecuteClientServer1 implements Runnable{
    ServerData data;
    Socket client;
    boolean banned;
    public ExecuteClientServer1(ServerData dat,Socket cl) throws IOException {
        super();
        data=dat;
        client=cl;
        if(data.serverStop){
            sendMessage("$SERVERSTOPPING$");
            try{
                Thread.sleep(50);
                client.close();
            }catch(Exception e){e.printStackTrace();}

        }
        if(data.ipBanList.get(client.getInetAddress().toString())!=null){
            sendMessage("$IPBANNED$");
            try {
                Thread.sleep(50);
                client.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            banned=true;
        }
        if(!banned&&!data.serverStop)data.out.println("有新的用户连接：" + client.getInetAddress() + ' '+client.getPort());
        if(!banned&&!data.serverStop)data.preparingSocket.add(client);
    }

    public void run() {

        if(!banned&&!data.serverStop)
        try {
            //拿到客户端输入流，读取用户信息
            Scanner scanner = new Scanner(client.getInputStream());
            String string = null;
            while(true){
                if(scanner.hasNext()) {
                    string = scanner.nextLine();
                    Pattern pattern = Pattern.compile("\r\n|\r|\n");
                    Matcher matcher = pattern.matcher(string);
                    string = matcher.replaceAll("");

                    if(string.startsWith("$REGISTER$")) {
                        String userName = string.split("\\$REGISTER\\$")[1];
                        if(data.clientList.get(userName)!=null){
                            sendMessage("$PLACED$");
                            continue;
                        }
                        if(data.banList.get(userName)!=null){
                            sendMessage("$BANNED$");
                            continue;
                        }
                        userRegister(userName, client);
                        data.infoList.put(userName,new ClientInfo(client.getInetAddress().toString(),
                                client.getPort()));
                        data.preparingSocket.remove(client);
                        continue;
                    }
                    else if(string.startsWith("$MESSAGE$")){
                        String message = string.split("\\$MESSAGE\\$",2)[1];
                        if(data.muteList.get(getUserName(client))!=null)continue;
                        sendToAll("用户"+getUserName(client)+"说:"+message);
                        data.out.println(CurDate.getDate()+"\n用户"+getUserName(client)+"说:"+message);
                        continue;

                    }
                    else if(string.equals("$LOGOFF$")){
                        String userName = getUserName(client);
                        data.out.println(CurDate.getDate()+"\n用户"+ userName +"下线了!!!");
                        sendToAll("[用户"+ userName +"下线了!!!]");
                        client.close();
                        data.clientList.remove(userName);
                        continue;
                    }
                }
            }
        }catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void sendMessage(String msg) throws IOException{
        PrintStream printStream = new PrintStream(client.getOutputStream(),true);
        printStream.println(msg);

    }
    public String getUserName(Socket socket) {
        String userName = null;
        for(String getKey : data.clientList.keySet()) {
            if(data.clientList.get(getKey).equals(socket)) {
                userName = getKey;
            }
        }
        return userName;
    }
    public void userRegister(String userName, Socket client) {
        data.out.println(CurDate.getDate());
        data.out.println("用户名为：" + userName);
        data.out.println("用户socket为：" + client);
        data.out.println("用户"+ userName +"上线了!");
        data.out.println("当前用户数为："+ (data.clientList.size()+1) +"人");
        sendToAll("用户"+ userName +"上线了!");
        data.clientList.put(userName, client);
    }
    public void sendToAll(String message) {

        for(Map.Entry<String, Socket> stringSocketEntry:data.clientList.entrySet()) {
            if(getUserName(client)!=null&&getUserName(client).equals(stringSocketEntry.getKey()))continue;
            try {
                Socket socket = stringSocketEntry.getValue();
                PrintStream printStream = new PrintStream(socket.getOutputStream(),true);
                printStream.println(message);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
}
class Connecting implements Runnable{
    ServerData data;
    public Connecting(ServerData dat) throws IOException {
        data=dat;

        data.out.println(CurDate.getDate()+"\n服务器已启动! 服务器IP:"+ InetAddress.getLocalHost().toString().split("/")[1]);
    }

    @Override
    public void run() {
        try {
            while(this.data.clientList.size()<20){
                Socket socket = data.serverSocket.accept();
                new Thread(new ExecuteClientServer1(data,socket)).start();
            }
        } catch (Exception e) {
        }
    }
}

public class Server {
    ServerData data;
    public Server(ServerData dat) throws IOException {
        data=dat;
        new Thread(new Connecting(data)).start();
    }
}
