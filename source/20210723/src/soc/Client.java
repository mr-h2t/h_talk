package soc;

import data.ClientData;
import data.CurDate;
import gui.DialogFrame;
import io.ClientToServer;

import java.io.IOException;
import java.io.PrintStream;
import java.net.ConnectException;
import java.net.Socket;
import java.util.Scanner;



class ReadFromServer implements Runnable{
    public ClientData data;
    public ReadFromServer(ClientData data) {
        super();
        this.data = data;
    }
    public void run() {
        try {
            Scanner scanner = new Scanner(data.client.getInputStream());
            scanner.useDelimiter("\n");
            while(scanner.hasNext()) {
                String string=scanner.nextLine();
                if(string.equals("$SHUTDOWN$")){
                    DialogFrame.showErrorDialog("服务器已关闭!!!");
                    Thread.sleep(1000);
                    data.client.close();
                    scanner.close();
                    System.exit(0);
                }
                else if(string.equals("$KICKED$")){
                    DialogFrame.showErrorDialog("您已被服务器踢出!!!");
                    Thread.sleep(1000);
                    data.client.close();
                    scanner.close();
                    System.exit(0);
                }
                else if(string.equals("$PLACED$")){
                    DialogFrame.showErrorDialog("该用户名已被占用!");
                }
                else if(string.equals("$BANNED$")){
                    DialogFrame.showErrorDialog("该用户已被封禁!!!");
                }
                else if(string.equals("$IPBANNED$")){
                    DialogFrame.showErrorDialog("该IP已被封禁!!!");
                    Thread.sleep(1000);
                    data.client.close();
                    scanner.close();
                    System.exit(0);
                }
                else if(string.equals("$SERVERSTOPPING$")){
                    DialogFrame.showErrorDialog("该服务器暂停开放!!!");
                    Thread.sleep(1000);
                    data.client.close();
                    scanner.close();
                    System.exit(0);
                }
                else if(string.equals("$MUTED$")){
                    DialogFrame.showErrorDialog("您被服务器禁言");
                    data.muted=true;
                }
                else if(string.equals("$UNMUTED$")){
                    DialogFrame.showErrorDialog("您被解除禁言");
                    data.muted=false;
                }
                else {
                    data.out.println(CurDate.getDate()+'\n'+string);

                }
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }

    }

}

public class Client {
    ClientData data;
    ClientToServer cts;
    public Client(ClientData data, ClientToServer cts) {
        this.data=data;
        this.cts=cts;
        try {
            this.data.client = new Socket(this.data.address, this.data.prt);
            this.data.out.println(CurDate.getDate()+"\n[成功登录服务器"+this.data.address+"]");
            this.cts.initPrintStream();
            this.cts.registerUser(this.data.registerName);
            Thread inThread = new Thread(new ReadFromServer(data));
            inThread.start();
        }
        catch(ConnectException e){
          //  JOptionPane.showMessageDialog(null,"无效IP地址",null,JOptionPane.ERROR_MESSAGE);
            DialogFrame.showErrorDialog("无效IP地址");
            System.exit(0);
        }catch (Exception e) {

            e.printStackTrace();
        }
    }
}